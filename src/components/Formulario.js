import React, {Fragment, useState} from 'react';
import {v4 as uuidv4} from 'uuid';
import PropTypes from "prop-types";

//crear un formulario de forma normal, los atributos van por linea, el input se cierra en una sola apertura.  Todo dentro del Fragment porque sino no se pueden retornar mas de un elemento.





const Formulario = ({crearCita}) => {

//Crear State para citas
//Arrancar el state con un objeto que tenga el name de los input implicados
//Utilizar el evento onChange en cada input para que se ejecute la funcion actualizarState
//Incluir denrto del actualizar State la funcion actualizar citas para modificar citas
//cuando pasamos por parametro e estamos invocando al evento que ejecuta la funcion por eso e.target.name seria el nombre del input y value el valor
//mediante array destructuring vamos a escribir el name del input y el valor y lo vamos a reescribir mediante el spray operator en un nuevo objeto actualizado...citas
//mediante array destructuring extraer los valores de citas, esto es para no tener que escribir citas.nombre, etc
//luego colocarlos como value en el input, va a permitir resetear el formulario a futuro
//Agregar el evento onSubmit al form para que realice lo que queramos que haga cuando el usuario pulse submit
//Agregar la foncuon submit cita, en ella pasaremos el evento por paramentro y haremos , con prevent default, que no lleve el comportamiento por default que hace que se envie el form.
//Se crea un nuevo state para el error, que arranca con error en false
//Vamos a empezar con la validacion, con IF el campo esta vacio (.trim()) se va a ejecutar la funcion que actualiza el state de error a true.
//dentro del return y invocando un ternario vamos a poner que si el error es true se muestre un P diciendo que rellenen los campos
//por fuera del IF de la validacion volveremos a poner en false a actualizarError asi se va el cartel de error
//vamos a asignar un id a la cita mediante la instalacion del mismo en consola npm i uuid y la importacion import {v4 as uuidv4} from 'uuid'; y posterior invocacion en el id  uuidv4() para que tenga un key unico
//recibir mediante props la funcion crearCita para poder agregar la cita al listado de citas completo
//para resetear los campos una ver completado y mandado el formulario usamos la funcion actualizarCitas y le pasasmos los valores en 0, los campos se formatean gracias a que asignamos el value de los input a estos elementos.
//mediante los propTypes que esta incluido dentro de react , documentamos los props de los componentes, es muy util a la hora de trabajar en equipo
const [cita, actualizarCitas] = useState({
    persona : "",
    tarea : "",
    fecha: "",
    hora: "",
    descripcion : ""  
})

const actualizarState=(e)=>{
    actualizarCitas({...cita,
[e.target.name] : e.target.value
   
})}



const { persona, tarea, fecha, hora, descripcion } =cita;


const [error, actualizarError] = useState(false)

const submitCita =(e)=>{
    e.preventDefault();
    //validacion
    if(persona.trim() === ''||tarea.trim() === ''||fecha.trim() === ''||hora.trim() === ''||descripcion.trim() === ''){
        actualizarError(true);
        return;
    }
//cita id
actualizarError(false)
cita.id = uuidv4();

//agregarcita
crearCita(cita)

//resetear campos
actualizarCitas({
    persona : "",
    tarea : "",
    fecha: "",
    hora: "",
    descripcion : ""  
})

}
    return ( 
    <Fragment>

        <h2>Crear tarea</h2>
        <form
        onSubmit={submitCita}>
    
    {error ===true ?
<p className="alerta-error">Todos los campos son obligatorios</p>: null
}
<select name="persona" 
className="u-full-width" 
onChange={actualizarState} 
value={persona}>

<option >Selecciona un miembro</option>

<option >MiembroA</option>

<option >MiembroB</option>

<option >MiembroC</option>

</select>

<label>Asunto</label>
<input
type="text"
name="tarea"
className="u-full-width"
placeholder="Titulo de la tarea"
onChange={actualizarState} 
value ={tarea}
/>


<label>Fecha</label>
<input
type="date"
name="fecha"
className="u-full-width"
onChange={actualizarState} 
value ={fecha}

/>


<label>Hora</label>
<input
type="time"
name="hora"
className="u-full-width"
onChange={actualizarState} 
value ={hora}
/>


<label>Descripción</label>
<textarea
name="descripcion"
className="u-full-width"
onChange={actualizarState} 
value ={descripcion}
></textarea>


<button
type="submit"
className="u-full-width button-primary"

>Agregar Cita</button>

        </form>
 
    </Fragment>
    );
}
 
Formulario.propTypes ={
crearCita : PropTypes.func.isRequired
}
export default Formulario;