import React  from 'react';
import PropTypes from "prop-types"


const Cita = ({cita, eliminarCita}) => {

//escribir los elementos de la cita
//Crear el boton eliminar, recordar que el evento onclick si no se invoca con ()=> eliminarCita() se ejecutaria solo
//mediante prop tomamos la funcion eliminarCita y le pasamos el id , 
//mediante los propTypes que esta incluido dentro de react , documentamos los props de los componentes, es muy util a la hora de trabajar en equipo
    return (  
<div className="cita">

<p>Nombre: <span>{cita.persona}</span></p> 
<p>Asunto: <span>{cita.tarea}</span></p> 
<p>Fecha: <span>{cita.fecha}</span></p> 
<p>Hora: <span>{cita.hora}</span></p> 
<p>descripcion: <span>{cita.descripcion}</span></p> 
<button
className="button eliminar u-full-width"
onClick={()=>eliminarCita(cita.id)}
>Eliminar &times;</button>
</div>
    )
}

Cita.propTypes ={
cita : PropTypes.object.isRequired,
eliminarCita: PropTypes.func.isRequired

}
export default Cita;