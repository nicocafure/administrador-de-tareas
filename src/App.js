import React,{Fragment,useState, useEffect} from 'react';
import Formulario from "./components/Formulario"
import Cita from "./components/Cita"
import Imagen from "./imagensvg.svg"



function App() {

  //1) Como no se pueden retornar dos elementos hacerlo con Fragment
 // En el App JS crear un nuevo useState que sera donde se guarden el conjunto de citas agregado
 // crear una funcion que lea las citas en el App para luego agregarlas al conjunto de citas , la funcions e llamara crear citas
 //  con la funcion que modifica el state de citas (guardar citas) , agregar la cita correspondiente con spread operator para que se vayan guardando
 // pasar mediante props la funcion crear cita para posteriormente ejecutarla en el formulario js 
 //mediante el map iterar las citas 
 //invocar al componente cita , y para que el mismo escriba los elementos de la cita pasarle mediante prop la "cita" y definir su key(muy importante a la hora de iterar)
//mediante la nueva funcion eliminarCita (que se crea en el app js por que es donde esta el array con las citas), tomamos el id
// crearemos un nuevo array "nuevasCitas" , donde mediante .filter vamos a dejar los objetos que tengan distinto id al que pasemos con el boton
//modificamos el array original citas mediante la funcion guardarCitas
//creamos una variable titulo que mediante un if y else en ternario dira un titulo u otro en base al numero de citas en el array mediante el metodo length
//utilizamos el hook useEffect el cual debe utilizarse siempre con una arrow function . El codigo se ejecutara cada vez que se ejecute la pagina, como un onload, la diferencia es que se puede utilizar
//cada vez que recarga un componente o algun elemento, al final al cerrar la funcion, se indica cual es el elemento (cambio en el mismo) que iniciara la funcion del useEffect, en este caso, un cambio en Citas
//con localStorage guardaremos los datos dentro de la computadora local
// con JSON.parse convertimos el arreglo dentro de un string para manipularlo , con getItem tomamos el item "citas de localStorage"
//modificamos dentro del useState de citas para que arranque con el arreglo previo almacenado en localStore
//En el useEffect modificamos el item "citas" de localstore mediante el setItem y el JSON.stringify y le pasamos el arreglo de citas cada vez que se modifica.

//citas en localstorage
let citasIniciales = JSON.parse(localStorage.getItem("citas"));
if(!citasIniciales){
  citasIniciales = []
}

let citasInicialesA = JSON.parse(localStorage.getItem("citasA"));
if(!citasInicialesA){
  citasInicialesA = []
}

let citasInicialesB = JSON.parse(localStorage.getItem("citasB"));
if(!citasInicialesB){
  citasInicialesB = []
}


//arreglo de citas enzo
const [citas, guardarCitas] =  useState(citasIniciales);

//arreglo de citas alvin

const [citasA, guardarCitasA] =  useState(citasInicialesA);

//arreglo de citas day

const [citasB, guardarCitasB] =  useState(citasInicialesB);




//funcion toma citas actuales y agregue la nueva
const crearCita = cita =>{

  if(cita.persona === "MiembroA"){
    guardarCitas([...citas,cita])
  }else if(cita.persona === "MiembroB"){
    guardarCitasA([...citasA,cita])
  }else if(cita.persona === "MiembroC"){
    guardarCitasB([...citasB,cita])
  }


}


const eliminarCita = id =>{
const nuevasCitas= citas.filter(cita=> cita.id !== id)
const nuevasCitasA= citasA.filter(cita=> cita.id !== id)
const nuevasCitasB= citasB.filter(cita=> cita.id !== id)

guardarCitas(nuevasCitas)
guardarCitasA(nuevasCitasA)
guardarCitasB(nuevasCitasB)

}
//para realizar ciertas operaciones cuando el state cambia
useEffect(() => {
  localStorage.setItem("citas", JSON.stringify(citas));
  localStorage.setItem("citasA", JSON.stringify(citasA));
  localStorage.setItem("citasB", JSON.stringify(citasB));

}, [citas,citasA,citasB])



const titulo = citas.length === 0 ? "No hay tareas para MiembroA" : "Tareas Miembro A"
const tituloA = citasA.length === 0 ? "No hay tareas para MiembroB" : "Tareas Miembro B"
const tituloB = citasB.length === 0 ? "No hay tareas para MiembroC" : "Tareas Miembro C"

  return (
    <Fragment>


    <div className="container">
    <h1>Administrador de Tareas</h1>
<div className="row">
<div className="one-half column">
<Formulario 
crearCita = {crearCita} />
</div>

<div className="contenedor-imagen one-half column">

<a href="#tareas" className="u-full-width "><button
type="submit"
className="u-full-width button-primary"
>Ver Tareas</button></a>
<img className="imagen" src={Imagen} alt="logo" />
</div>


</div>

<div className="row">
<div 
className="lista-tareas four columns"
id="tareas"
>
<h4>{titulo}</h4>
{citas.map(cita=> (
<Cita 
cita= {cita}
key= {cita.id}
eliminarCita= {eliminarCita}
/>
))}
</div>

<div className="lista-tareas four columns">
<h4>{tituloA}</h4>
{citasA.map(cita=> (
<Cita 
cita= {cita}
key= {cita.id}
eliminarCita= {eliminarCita}
/>
))}
</div>

<div className="lista-tareas four columns">
<h4>{tituloB}</h4>
{citasB.map(cita=> (
<Cita 
cita= {cita}
key= {cita.id}
eliminarCita= {eliminarCita}
/>
))}
</div>



</div>
    </div>
    </Fragment>
  );
}

export default App;
